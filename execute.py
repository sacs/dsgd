from main import launch
import constants
import argparse
import os

my_parser = argparse.ArgumentParser()

my_parser.add_argument(
    '-d',
    '--dataset',
    action='store',
    type=str,
    choices=['mnist', 'cifar10'],
    default='mnist',
    help='Dataset on which to run experiment.')

my_parser.add_argument(
    '-sc',
    '--scale',
    action='store_true',
    default=False,
    help='Set to True to scale images by 1.0/255')

my_parser.add_argument(
    '-cat',
    '--categorical',
    action='store_true',
    default=False,
    help='Set to True if using categorical labels. Default is False.')

my_parser.add_argument(
    '-td',
    '--test_dataset_size',
    action='store',
    type=int,
    default=-1,
    help='Set reduced size for test dataset. -1 means use full size.')

my_parser.add_argument(
    '-b',
    '--batch_size',
    action='store',
    type=int,
    default=64,
    help='Training batch size.')

my_parser.add_argument(
    '-iid',
    '--iid_data_partitioning',
    action='store_true',
    default=False,
    help='Whether data partitioning is IID. Default is False.')

my_parser.add_argument(
    '-e',
    '--num_epochs',
    action='store',
    type=int,
    required=True,
    help='Number of epochs.')

my_parser.add_argument(
    '-rep',
    '--repetitions',
    action='store',
    type=int,
    default=1,
    help='Number of times to repeat the experiment.')

my_parser.add_argument(
    '-r',
    '--radius',
    action='store',
    type=float,
    default=1.0,
    help='Radius for projection. Default is 1.0.')

my_parser.add_argument(
    '-p',
    '--project',
    action='store_true',
    default=False,
    help='Whether to project. Default is False.')

my_parser.add_argument(
    '-proj_to_epoch',
    action='store',
    type=int,
    default=-1,
    help='After how many epochs to project.')

my_parser.add_argument(
    '-steps_before_gossip',
    action='store',
    type=int,
    default=-1,
    help='Number of steps between gossip.')

my_parser.add_argument(
    '-initialise_after_gossip',
    action='store_true',
    default=False,
    help='Whether to reset optimizer state after gossip. Default is False.')

my_parser.add_argument(
    '-m',
    '--model',
    action='store',
    type=str,
    choices=['model_linear_1', 'model_linear_2', 'gn_lenet'],
    default='model_linear_1',
    help='Choice of model.')

my_parser.add_argument(
    '-lr',
    '--learning_rate',
    action='store',
    type=float,
    default=0.001,
    help='Learning rate for training.')

my_parser.add_argument(
    '-rho',
    '--momentum',
    action='store',
    type=float,
    default=0.0,
    help='Momentum parameter.')

my_parser.add_argument(
    '-t',
    '--topology',
    action='store',
    type=str,
    choices=['ring', 'grid', 'complete', 'd_regular'],
    default='ring',
    help='Choice of topology')

my_parser.add_argument(
    '-n',
    '--num_nodes',
    action='store',
    type=int,
    default=32,
    help='Number of nodes.')

my_parser.add_argument(
    '-dg',
    '--degree',
    action='store',
    type=int,
    default=5,
    help='Degree if topology chosen is d_regular graph')

my_parser.add_argument(
    '-l',
    '--log_dir',
    action='store',
    type=str,
    default='my_run',
    help='Title for logging directory.')

my_parser.add_argument(
    '-c',
    '--csv_logger',
    action='store_true',
    default=False,
    help='Whether using CSV Logger. Default is False.')


args = my_parser.parse_args()
for k,v in vars(args).items():
    print(k, ":", v)

logging_path = os.path.join(constants.SAVE_FOLDER, args.log_dir)
if not os.path.exists(logging_path):
    os.makedirs(logging_path)

with open(os.path.join(logging_path, 'params.csv'), 'w') as f:
    for k,v in vars(args).items():
        f.write(str(k) + "," + str(v) + "\n")

run = {
    'title': args.log_dir,
    'csv_logger': args.csv_logger,
    'n_nodes': args.num_nodes,
    'topology': {
        'name': args.topology
    },
    'model': {
        'name': args.model,
        'lr': args.learning_rate,
        'momentum': args.momentum
    },
    'dataset': {
        'name': args.dataset,
        'scale': args.scale,
        'categorical': args.categorical,
        'test_dataset_size': args.test_dataset_size,
        'batch_size': args.batch_size,
        'partition_method': {
            'name': 'partition', 
            'sort_dataset': not args.iid_data_partitioning
        }
    },
    'general': {
        'n_epochs': args.num_epochs,
        'repetitions': args.repetitions,
        'projection': args.project,
        'radius': args.radius,
        'proj_to_epoch': args.proj_to_epoch, 
        'steps_before_gossip': args.steps_before_gossip,
        'initialize_after_gossip': args.initialise_after_gossip
    }
}

if args.topology == 'd_regular':
    run['topology']['degree'] = args.degree

launch(run)