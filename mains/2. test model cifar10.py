from numpy import fabs
from Logger import Logger
from tensorflow import keras

import dataset_formatter
import models
import topologies as tp
import utils
from DSGD import DSGD

n = 1
n_epochs = 5
graph = tp.ring(n)
n_nodes = graph.number_of_nodes()

data_iid = True
repetitions = 1
projections = [False]
# dividers = [2, 5, 10, 20]
dividers = [1]

train_datasets, test_dataset = dataset_formatter.get_cifar10_for_nodes(n_nodes, iid=data_iid, scale=True)
model_getter = models.cifar_cnn_88
metric_getter = keras.metrics.CategoricalAccuracy  

base_norm = 0
for proj in projections:
    for div in dividers:
        rad = base_norm / div
        logger = Logger()
        logger.log_metadata(
            exp_name='test model cifar10',
            num_nodes=n_nodes,
            num_epochs=n_epochs,
            dataset_name='cifar10',
            topology='ring',
            model_name='cifar_cnn_88',
            data_iid=data_iid,
            projection=proj,
            radius=rad,
            description='base_norm: {base_norm}'
        )
        for rep in range(repetitions):
            print(f'Projection {proj}, divider {div}, repetition {rep}')
            logger.currentRepetition = rep
            nodes, average_node = utils.fill_nodes(n_nodes, model_getter, train_datasets, test_dataset, metric_getter, logger)
            dsgd = DSGD(n_nodes, ball_projection=proj, ball_radius=rad)
            dsgd.set_graph(graph)
            dsgd.set_nodes(nodes)
            dsgd.set_average_node(average_node)
            dsgd.train(n_epochs)

            logger.save()

        # Do one radius if no projection 
        if proj is False:
            base_norm = dsgd.get_average_node_norm()
            break
        
