import tensorflow as tf
import tensorflow_addons as tfa
from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Dense, Flatten, Dropout

from ResNet import resnet_v1, resnet_v2

"""
Best practice: better to create a new model than substantially modifying one
"""


models_list = dict()


def register_model(name):
    """ 
    Decorator that add all the models in the global dictionnary "models_list"
    """
    global models_list

    def f(func):
        models_list[name] = func
        return func

    return f


def is_model(name):
    global models_list
    return name in models_list


def get_model(name, *args, **kwargs):
    """
    Get a graph based on the name of the topology
    """
    global models_list
    if name not in models_list:
        raise ValueError(f'Model not implemented: {name}')
    return models_list[name](*args, **kwargs)


@register_model('model_linear_1')
def get_model_linear_1(lr, momentum, activation="relu", shapes=(64, 10)):
    def model_linear_1():
        optimizer = keras.optimizers.SGD(learning_rate=lr, momentum=momentum)
        loss_fn = keras.losses.SparseCategoricalCrossentropy(from_logits=True)
        # No need to say the format of the input data
        model = keras.Sequential([
            Flatten(),
            Dense(shapes[0], activation=activation, name="dense_1", use_bias=True),
            Dense(shapes[1], name="predictions", use_bias=True)
        ])
        return optimizer, model, loss_fn, keras.metrics.SparseCategoricalAccuracy

    return model_linear_1

@register_model('model_linear_2')
def get_model_linear_1(lr, momentum, activation="relu", shapes=(64, 64, 10)):
    def model_linear_1():
        optimizer = keras.optimizers.SGD(learning_rate=lr, momentum=momentum)
        loss_fn = keras.losses.SparseCategoricalCrossentropy(from_logits=True)
        # No need to say the format of the input data
        model = keras.Sequential([
            Flatten(),
            Dense(shapes[0], activation=activation, name="dense_1", use_bias=True),
            Dense(shapes[1], activation=activation, name="dense_2", use_bias=True),
            Dense(shapes[2], name="predictions", use_bias=True)
        ])
        return optimizer, model, loss_fn, keras.metrics.SparseCategoricalAccuracy

    return model_linear_1

@register_model('simple_MNSIT_convnet_2')
def get_simple_MNSIT_convnet_2(lr, momentum, r):
    def simple_MNSIT_convnet_2():
        optimizer = keras.optimizers.SGD(learning_rate=lr, momentum=momentum)
        loss_fn = keras.losses.CategoricalCrossentropy()

        model = keras.Sequential([
            Conv2D(16, (3, 3), activation='relu',
                   kernel_regularizer=tf.keras.regularizers.l2(r),
                   bias_regularizer=tf.keras.regularizers.l2(r)),
            MaxPooling2D((3, 3)),
            Flatten(),
    #        Dense(100,activation='relu'),
            Dense(10, activation='softmax')
        ])
        return optimizer, model, loss_fn, keras.metrics.CategoricalAccuracy

    return simple_MNSIT_convnet_2


@register_model('simple_MNSIT_convnet_2_adam')
def get_simple_MNSIT_convnet_2(lr, r):
    def simple_MNSIT_convnet_2():
        optimizer = keras.optimizers.Adam(learning_rate=lr)
        loss_fn = keras.losses.CategoricalCrossentropy()

        model = keras.Sequential([
            Conv2D(16, (3, 3), activation='relu',
                   kernel_regularizer=tf.keras.regularizers.l2(r),
                   bias_regularizer=tf.keras.regularizers.l2(r)),
            MaxPooling2D((3, 3)),
            Flatten(),
            Dense(10, activation='softmax')
        ])
        return optimizer, model, loss_fn, keras.metrics.CategoricalAccuracy

    return simple_MNSIT_convnet_2


@register_model('simple_MNIST_convnet')
def get_simple_MNIST_convnet(lr):
    def simple_MNIST_convnet():
        optimizer = keras.optimizers.Adam(learning_rate=lr)
        loss_fn = keras.losses.CategoricalCrossentropy()
        model = keras.Sequential([
            layers.Conv2D(32, kernel_size=(3, 3), activation="relu"),
            layers.MaxPooling2D(pool_size=(2, 2)),
            layers.Conv2D(64, kernel_size=(3, 3), activation="relu"),
            layers.MaxPooling2D(pool_size=(2, 2)),
            layers.Flatten(),
            layers.Dropout(0.5),
            layers.Dense(10, activation="softmax"),
        ])
        return optimizer, model, loss_fn, keras.metrics.CategoricalAccuracy

    return simple_MNIST_convnet


@register_model('model_cnn_1')
def get_model_cnn_1(lr, momentum):
    def model_cnn_1():
        model = keras.models.Sequential([
            Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same',
                   input_shape=(32, 32, 3)),
            Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'),
            MaxPooling2D((2, 2)),
            Flatten(),
            Dense(128, activation='relu', kernel_initializer='he_uniform'),
            Dense(10, activation='softmax'),
        ])
        opt = keras.optimizers.SGD(learning_rate=lr, momentum=momentum)
        loss_fn = keras.losses.CategoricalCrossentropy()
        return opt, model, loss_fn, keras.metrics.CategoricalAccuracy

    return model_cnn_1


@register_model('gn_lenet')
def get_gn_lenet(lr, momentum):
    def gn_lenet():
        model = keras.models.Sequential([
            Conv2D(32, (5, 5), kernel_initializer='he_uniform', padding='same'),
            # tfa.layers.GroupNormalization(32),
            MaxPooling2D(pool_size=3, strides=2),
            layers.ReLU(),

            Conv2D(32, (5, 5), kernel_initializer='he_uniform', padding='same'),
            tfa.layers.GroupNormalization(32),
            MaxPooling2D(pool_size=3, strides=2),
            layers.ReLU(),

            Conv2D(64, (5, 5), kernel_initializer='he_uniform', padding='same'),
            tfa.layers.GroupNormalization(64),
            MaxPooling2D(pool_size=3, strides=2),
            layers.ReLU(),

            Flatten(),
            Dense(10, activation='softmax', kernel_initializer='he_uniform'),

        ])
        opt = keras.optimizers.SGD(learning_rate=lr, momentum=momentum)
        loss_fn = keras.losses.CategoricalCrossentropy()
        return opt, model, loss_fn, keras.metrics.CategoricalAccuracy

    return gn_lenet


@register_model('cifar_cnn_88')
def get_cifar_cnn_88(lr):
    def cifar_cnn_88():
        """
        source https://www.kaggle.com/ektasharma/simple-cifar10-cnn-keras-code-with-88-accuracy
        """
        model = keras.models.Sequential()

        model.add(layers.Conv2D(32, (3, 3), padding='same', activation='relu', input_shape=(32, 32, 3)))
        model.add(layers.BatchNormalization())
        model.add(layers.Conv2D(32, (3, 3), padding='same', activation='relu'))
        model.add(layers.BatchNormalization())
        model.add(layers.MaxPooling2D(pool_size=(2, 2)))
        model.add(layers.Dropout(0.3))

        model.add(layers.Conv2D(64, (3, 3), padding='same', activation='relu'))
        model.add(layers.BatchNormalization())
        model.add(layers.Conv2D(64, (3, 3), padding='same', activation='relu'))
        model.add(layers.BatchNormalization())
        model.add(layers.MaxPooling2D(pool_size=(2, 2)))
        model.add(layers.Dropout(0.5))

        model.add(layers.Conv2D(128, (3, 3), padding='same', activation='relu'))
        model.add(layers.BatchNormalization())
        model.add(layers.Conv2D(128, (3, 3), padding='same', activation='relu'))
        model.add(layers.BatchNormalization())
        model.add(layers.MaxPooling2D(pool_size=(2, 2)))
        model.add(layers.Dropout(0.5))

        model.add(layers.Flatten())
        model.add(layers.Dense(128, activation='relu'))
        model.add(layers.BatchNormalization())
        model.add(layers.Dropout(0.5))
        model.add(layers.Dense(10, activation='softmax'))

        optimizer = keras.optimizers.Adam(learning_rate=lr)
        loss_fn = keras.losses.CategoricalCrossentropy()

        return optimizer, model, loss_fn, keras.metrics.CategoricalAccuracy

    return cifar_cnn_88


@register_model('resnet_v2')
def get_resnet_v2(lr, input_shape=(32, 32, 3), depth=20):
    def resnet():
        optimizer = keras.optimizers.Adam(learning_rate=lr)
        loss_fn = keras.losses.CategoricalCrossentropy()
        return optimizer, resnet_v2(input_shape, depth, num_classes=10), loss_fn, keras.metrics.CategoricalAccuracy

    return resnet


@register_model('resnet_v1')
def get_resnet_v1(lr, input_shape=(32, 32, 3), depth=20):
    def resnet():
        optimizer = keras.optimizers.Adam(learning_rate=lr)
        loss_fn = keras.losses.CategoricalCrossentropy()
        return optimizer, resnet_v1(input_shape, depth, num_classes=10), loss_fn, keras.metrics.CategoricalAccuracy

    return resnet


@register_model('resnet_v1_sgd')
def get_resnet_v1(lr, input_shape=(32, 32, 3), depth=20):
    def resnet():
        optimizer = keras.optimizers.SGD(learning_rate=lr, momentum=0.9, nesterov=True) # From consensus distance paper
        loss_fn = keras.losses.CategoricalCrossentropy()
        return optimizer, resnet_v1(input_shape, depth, num_classes=10), loss_fn, keras.metrics.CategoricalAccuracy

    return resnet


@register_model('cifar_cnn_84')
def get_cifar_cnn_84l(lr, momentum):
    def cifar_cnn_84():
        model = keras.models.Sequential()
        model.add(Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same', input_shape=(32, 32, 3)))
        model.add(Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'))
        model.add(MaxPooling2D((2, 2)))
        model.add(Dropout(0.2))
        model.add(Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'))
        model.add(Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'))
        model.add(MaxPooling2D((2, 2)))
        model.add(Dropout(0.2))
        model.add(Conv2D(128, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'))
        model.add(Conv2D(128, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'))
        model.add(MaxPooling2D((2, 2)))
        model.add(Dropout(0.2))
        model.add(Flatten())
        model.add(Dense(128, activation='relu', kernel_initializer='he_uniform'))
        model.add(Dropout(0.2))
        model.add(Dense(10, activation='softmax'))

        opt = keras.optimizers.SGD(learning_rate=lr, momentum=momentum)
        loss_fn = keras.losses.CategoricalCrossentropy()
        return opt, model, loss_fn, keras.metrics.CategoricalAccuracy

    return cifar_cnn_84


class ElementWiseMultiplication(keras.layers.Layer):
    def __init__(self):
        super(ElementWiseMultiplication, self).__init__()

    def build(self, input_shape):
        w_init = tf.keras.initializers.GlorotUniform()
        self.w = tf.Variable(
            initial_value=w_init(shape=(input_shape[-1],)),
            trainable=True
        )

    def call(self, inputs):
        return tf.math.multiply(inputs, self.w)


@register_model('mse_sanity_check')
def get_mse_model(lr, momentum):
    def mse_model():
        model = keras.models.Sequential([
            ElementWiseMultiplication()
            ])

        opt = tf.keras.optimizers.SGD(learning_rate=lr, momentum=momentum)
        loss_fn = tf.keras.losses.MeanSquaredError()
        return opt, model, loss_fn, keras.metrics.MeanSquaredError

    return mse_model
        
