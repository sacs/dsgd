
def average_weights_new_version(self):
    """ Written to check the correctness of the average_weights function. ok but slower."""
    models_parameters = [node.model.get_weights() for node in self.nodes]
    models_parameters_updated = compute_gossip(self.n_nodes, models_parameters, self.averaging_weights)
    if self.ball_projection:
        models_parameters_updated = project_models(models_parameters_updated, self.ball_radius)
    average_model_parameters_updated = compute_average_model(self.n_nodes, models_parameters_updated)

    for node, parameters in zip(self.nodes, models_parameters_updated):
        node.model.set_weights(parameters)
    self.average_node.model.set_weights(average_model_parameters_updated)


def compute_gossip(n_nodes, models_parameters, averaging_weights):
    averaging_weights *= n_nodes
    new_models_parameters = []
    for neighbors in averaging_weights:
        new_params = sum_models(models_parameters, weights=neighbors)
        new_params = [w / n_nodes for w in new_params]
        new_models_parameters.append(new_params)
    return new_models_parameters


def project_models(models_parameters, radius):
    new_parameters = [DSGD.project_ball(parameters, radius) for parameters in models_parameters]
    return new_parameters


def compute_average_model(n_nodes, models_parameters):
    sum_model_parameters = sum_models(models_parameters)
    average_model_parameters = [layer / n_nodes for layer in sum_model_parameters]
    return average_model_parameters


def sum_models(models_parameters, weights=None):
    if weights is not None:
        assert len(models_parameters) == len(weights)
    else:
        weights = np.ones(shape=len(models_parameters))

    sum_parameters = [np.zeros(shape=layer.shape) for layer in models_parameters[0]]
    for param, ponderation in zip(models_parameters, weights):
        for i, layer in enumerate(param):
            sum_parameters[i] += ponderation * layer
    return sum_parameters
