from Node_simulator import NodeSimulator
from models import is_model
from dataset_formatter import is_dataset
from topologies import is_topology


def fill_nodes(n_nodes, model_getter, train_datasets, test_dataset, logger, lr_decay_func=None):
    """
    :param n_nodes: number of nodes to create
    :param model_getter: function to call to get optimizer, model, loss, metric_getter
    :param train_datasets: list of dataset for each node
    :param test_dataset: test dataset (same for each node)
    :param metric_getter: function to call to get a metric
    :param logger: Logger object to log metrics
    :return: list of filled nodes, average node which will compute the average model of all nodes
    """
    nodes = []
    for i in range(n_nodes):
        node_name = f'node_{i}'
        node = NodeSimulator(node_name, lr_decay_func)
        node.logger = logger
        node.optimizer, node.model, node.loss_fn, metric_getter = model_getter()
        node.train_dataset, node.test_dataset = train_datasets[i], test_dataset
        node.val_accuracy = metric_getter()
        node.epoch_train_accuracy = metric_getter()
        node.generate_weights()
        nodes.append(node)

    average_node = NodeSimulator('average_node')
    average_node.logger = logger
    average_node.optimizer, average_node.model, average_node.loss_fn, metric_getter = model_getter()
    average_node.test_dataset = test_dataset
    average_node.val_accuracy = metric_getter()
    average_node.epoch_train_accuracy = metric_getter()
    average_node.generate_weights()

    return nodes, average_node


def check_correctness(topo_name, model_name, dataset_name):
    if not (is_topology(topo_name) and is_model(model_name) and is_dataset(dataset_name)):
        return False
    return True
