import sys, os

from Logger import Logger
from CSVLogger import CSVLogger
from tensorflow import keras
import tensorflow as tf
import numpy as np

import constants
import dataset_formatter
import models
import topologies as tp
import utils
from DSGD import DSGD


def launch(run):
    tf.random.set_seed(constants.SEED)
    np.random.seed(constants.SEED)

    tf.keras.backend.set_floatx('float64')
    # sys.stdout = open(os.path.join(constants.SAVE_FOLDER, str(os.getpid()) + ".out"), "w")

    n_nodes = run['n_nodes']
    topology_info = run['topology']
    model_info = run['model']
    dataset_info = run['dataset']
    run_info = run['general']
    n_epochs, repetitions, proj, radius, proj_to_epoch, MWU = run_info['n_epochs'], run_info['repetitions'], \
        run_info['projection'], run_info['radius'], run_info['proj_to_epoch'],run_info['MWU']


    graph = tp.get_graph(**topology_info, n_nodes=n_nodes)
    n_nodes = graph.number_of_nodes()
    train_datasets, test_dataset = dataset_formatter.get_datasets(n_nodes=n_nodes, **dataset_info)
    model_getter = models.get_model(**model_info)

    if run['csv_logger']:
        logger = CSVLogger()
    else:
        logger = Logger()
    
    logger.log_metadata(
        run=run
    )

    assert repetitions > 0
    for rep in range(repetitions):
        print(f'Settings: {run}')
        print(f'repetition: {rep}')
        logger.currentRepetition = rep
        nodes, average_node = utils.fill_nodes(n_nodes, model_getter, train_datasets, test_dataset, logger)
        dsgd = DSGD(n_nodes, run_info, logger=logger)
        dsgd.set_graph(graph)
        dsgd.set_nodes(nodes)
        dsgd.set_average_node(average_node)
        dsgd.train(n_epochs,MWU)

        logger.save()

    # Do one radius if no projection
    if proj is False and not run['csv_logger']:
        base_norm = dsgd.get_average_node_norm()
        logger.set_basenorm(base_norm)
        logger.save()

    # send graph at the end of repetitions
    if not run['csv_logger']:
        logger.send_graph()  
    