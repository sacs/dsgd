from re import I
import tensorflow as tf
import tensorflow_datasets as tfds
from tensorflow.keras.utils import to_categorical
import numpy as np
import constants


def shorten_dataset(x_train, y_train, x_test, y_test):
    n_train, n_test = int(len(x_train) / 10), int(len(x_test) / 10)
    x_train, y_train = x_train[:n_train], y_train[:n_train]
    x_test, y_test = x_test[:n_test], y_test[:n_test]
    return x_train, y_train, x_test, y_test


def get_mnist(scale=False, categorical=False):
    """
    Retrieve the MNIST dataset under the form of numpy arrays, where each image is flattened
    :return: (x_train, y_train), (x_test, y_test) numpy arrays
    """
    (x_train, y_train), (x_test, y_test) = tfds.as_numpy(tfds.load(
        'mnist',
        split=['train', 'test'],
        batch_size=-1,
        as_supervised=True,
        shuffle_files=False
    ))

    if categorical:
        y_train = to_categorical(y_train)
        y_test = to_categorical(y_test)


    x_train = x_train.astype("float32")
    x_test = x_test.astype("float32")
    if scale:
        x_train = x_train / 255
        x_test = x_test / 255

    # For debug purposes
    if constants.DEBUG:
        x_train, y_train, x_test, y_test = shorten_dataset(x_train, y_train, x_test, y_test)
    return (x_train, y_train), (x_test, y_test)


def get_emnist(scale=False):
    """
    Retrieve the EMNIST dataset under the form of numpy arrays, where each image is flattened
    :return: (x_train, y_train), (x_test, y_test) numpy arrays
    """
    (x_train, y_train), (x_test, y_test) = tfds.as_numpy(tfds.load(
        'emnist/digits',
        split=['train', 'test'],
        batch_size=-1,
        as_supervised=True,
        shuffle_files=False
    ))

    x_train = x_train.astype("float32")
    x_test = x_test.astype("float32")
    if scale:
        x_train = x_train / 255
        x_test = x_test / 255
    
    # For debug purposes
    if constants.DEBUG:
        x_train, y_train, x_test, y_test = shorten_dataset(x_train, y_train, x_test, y_test)
    return (x_train, y_train), (x_test, y_test)


def get_cifar10(scale=False):
    """
    Retrieve the CIFAR10 dataset under the form of numpy arrays, where each image is flattened
    :return: (x_train, y_train), (x_test, y_test) numpy arrays
    """
    (x_train, y_train), (x_test, y_test) = tfds.as_numpy(tfds.load(
        'cifar10',
        split=['train', 'test'],
        batch_size=-1,
        as_supervised=True,
        shuffle_files=False
    ))

    y_train = to_categorical(y_train)
    y_test = to_categorical(y_test)

    x_train = x_train.astype("float32")
    x_test = x_test.astype("float32")
    if scale:
        x_train = x_train / 255
        x_test = x_test / 255

    # For debug purposes
    if constants.DEBUG:
        x_train, y_train, x_test, y_test = shorten_dataset(x_train, y_train, x_test, y_test)
    return (x_train, y_train), (x_test, y_test)


def generate_per_node_datasets(x_train, y_train, x_test, y_test, n_nodes, batch_size=64, iid=False):
    """
    Generate num_nodes datasets with the given inputs
    :return: num_nodes datasets, one for each node
    """
    tf.random.set_seed(constants.SEED)  # Shuffling always done the same way

    train_datasets = []
    train_dataset_x = np.array_split(x_train, n_nodes)
    train_dataset_y = np.array_split(y_train, n_nodes)

    buff_size = len(x_train) // n_nodes

    for i in range(n_nodes):
        dataset = tf.data.Dataset.from_tensor_slices((train_dataset_x[i], train_dataset_y[i]))
        dataset = dataset.shuffle(buffer_size=buff_size).batch(batch_size)
        train_datasets.append(dataset)
    test_dataset = tf.data.Dataset.from_tensor_slices((x_test, y_test))
    test_dataset = test_dataset.shuffle(buffer_size=buff_size).batch(batch_size)
    return train_datasets, test_dataset

def sort_mnist(x, y):
    """
    y is a vector of classes
    """
    sorting = np.argsort(y)
    x = x[sorting]
    y = y[sorting]
    return x, y


def sort_cifar10(x, y):
    """
    y is a matrix (one-hot-encoding) of classes
    """
    sorting = np.argsort(np.argmax(y, axis=1))
    x = x[sorting]
    y = y[sorting]
    return x, y


def get_mnist_for_nodes(n_nodes, batch_size=64, iid=True, scale=False, categorical=False):
    (x_train, y_train), (x_test, y_test) = get_mnist(scale, categorical)
    # In the non iid case we first order the data by class before doing the splitting
    if not iid:
        if categorical:
            x_train, y_train = sort_cifar10(x_train, y_train)
        else:
            x_train, y_train = sort_mnist(x_train, y_train)
    return generate_per_node_datasets(x_train, y_train, x_test, y_test, n_nodes, batch_size, iid)


def get_cifar10_for_nodes(n_nodes, batch_size=64, iid=True, scale=False):
    (x_train, y_train), (x_test, y_test) = get_cifar10(scale)
    # In the non iid case we first order the data by class before doing the splitting
    if not iid:
        x_train, y_train = sort_cifar10(x_train, y_train)
    return generate_per_node_datasets(x_train, y_train, x_test, y_test, n_nodes, batch_size)
