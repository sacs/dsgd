import tensorflow as tf
from tensorflow.python.training.tracking.base import VARIABLE_VALUE_KEY
import tensorflow_datasets as tfds
from tensorflow.keras.utils import to_categorical
import numpy as np
import constants
from scipy.stats import norm

available_datasets = {'mnist', 'cifar10', 'emnist/digits', 'mse_sanity_check'}


def is_dataset(name):
    global available_datasets
    return name in available_datasets


def get_numpy_from_tfds(dataset_name, scale=None, categorical=None):
    """
    Retrieve the dataset_name dataset under the form of numpy arrays
    :return: (x_train, y_train), (x_test, y_test) numpy arrays
    """
    if constants.DEBUG:
        print('DEBUG, 10% OF DATASET USED')
        split = ['train[:10%]', 'test[:10%]']
    else:
        split = ['train', 'test']

    (x_train, y_train), (x_test, y_test) = tfds.as_numpy(tfds.load(
        dataset_name,
        split=split,
        batch_size=-1,
        as_supervised=True,
        shuffle_files=False
    ))

    if categorical:
        y_train = to_categorical(y_train)
        y_test = to_categorical(y_test)

    x_train = x_train.astype("float64")
    x_test = x_test.astype("float64")
    if scale:
        x_train = x_train / 255
        x_test = x_test / 255

    return (x_train, y_train), (x_test, y_test)


def choose_index(size, class_to_index, distribution, repetition, method='exact_distribution'):
    """
    Pick size indices, sampled in class_to_index according to distribution.
    E.g: size=10, class_to_index = {0: [10], 1: [20]}, distribution = [0.1, 0.9]
    will yield an array looking like [10, 20, 20, 20, 20, 20, 20, 20, 20, 20]
    """
    pick_index = np.array([], dtype=np.int64)
    classes = np.arange(len(distribution))

    if method == 'exact_distribution':
        # The histogram of the labels will look exacly like the provided distribution.
        # The total number of examples may be wrong, i.e len(pick_index) != size
        element_per_class = np.array(distribution) * size
        element_per_class = np.round(element_per_class).astype(
            np.int64)  # This causes no harm is the number of classes << size
        for c, number in zip(classes, element_per_class):
            i = np.random.choice(class_to_index[c], size=number, replace=repetition)
            pick_index = np.concatenate((pick_index, i))

    else:
        a = np.random.choice(classes, size=size, p=distribution)
        cl, count = np.unique(a, return_counts=True)
        for c, number in zip(cl, count):
            i = np.random.choice(class_to_index[c], size=number, replace=repetition)
            pick_index = np.concatenate((pick_index, i))

    return pick_index


def reduce_dataset(x, y, size):
    if size is None or size != -1:
        if size > y.shape[0]:
            raise ValueError('The size of the test dataset is at most the size of the original test dataset')
        indices = np.arange(0, y.shape[0])
        indices_to_pick = np.random.choice(indices, size, replace=False)
        x = x[indices_to_pick]
        y = y[indices_to_pick]
    return x, y


def generate_target(n_dimensions, max_norm):
    """ Generates a random vector for n_dimensions dimensions with a norm uniformly sampled in [0, max_norm] """
    target = np.random.uniform(size=n_dimensions)
    norm = np.random.uniform(low=0, high=max_norm)
    target = target / np.linalg.norm(target) * norm
    return target

def get_sanity_datasets(n_nodes, test_dataset_size, batch_size, partition_method):

    train_dataset_size, n_dimensions, max_norm = partition_method['train_dataset_size'], partition_method['n_dimensions'], partition_method['max_norm']
    if partition_method['name'] == 'same_objective':
        target = generate_target(n_dimensions, max_norm)
        train_datasets = []
        for _ in range(n_nodes):
            x = np.ones(shape=(train_dataset_size, n_dimensions))
            y = np.tile(target, (train_dataset_size, 1))
            dataset = tf.data.Dataset.from_tensor_slices((x, y))
            dataset = dataset.shuffle(buffer_size=train_dataset_size).batch(batch_size)
            train_datasets.append(dataset)

        x_test = np.ones(shape=(test_dataset_size, n_dimensions))
        y_test = np.tile(target, (test_dataset_size, 1))
        test_dataset = tf.data.Dataset.from_tensor_slices((x_test, y_test))
        return train_datasets, test_dataset

    elif partition_method['name'] == 'different_uniform':
        train_datasets = []
        targets = []
        for _ in range(n_nodes):
            target = generate_target(n_dimensions, max_norm)
            targets.append(target)
            x = np.ones(shape=(train_dataset_size, n_dimensions))
            y = np.tile(target, (train_dataset_size, 1))
            dataset = tf.data.Dataset.from_tensor_slices((x, y))
            dataset = dataset.shuffle(buffer_size=train_dataset_size).batch(batch_size)
            train_datasets.append(dataset)

        target = np.mean(targets, axis=0)
        x_test = np.ones(shape=(test_dataset_size, n_dimensions))
        y_test = np.tile(target, (test_dataset_size, 1))
        test_dataset = tf.data.Dataset.from_tensor_slices((x_test, y_test))
        return train_datasets, test_dataset


    else:
        raise KeyError(f"Partition method {partition_method['name']} not available for sanity check")


def get_datasets(name, n_nodes, categorical, scale, test_dataset_size, batch_size, partition_method):
    if not is_dataset(name):
        raise KeyError(f'Dataset "{name}" not available')

    if name == 'mse_sanity_check':
        return get_sanity_datasets(n_nodes, test_dataset_size, batch_size, partition_method)

    (x_train, y_train), (x_test, y_test) = get_numpy_from_tfds(name, scale)

    x_test, y_test = reduce_dataset(x_test, y_test, test_dataset_size)  # Reduce the size of the test dataset
    if categorical:  # Transform in one-hot encoding
        y_test = to_categorical(y_test, num_classes=10)

    # fill the class_to_index
    # class_to_index[class_id] = list of index in x_train with same class
    class_to_index = dict()
    classes = np.unique(y_train)
    for cl in classes:
        class_to_index[cl] = (y_train == cl).nonzero()[0]

    x_train_split, y_train_split = [], []
    split_type = partition_method['name']
    if split_type == 'partition':  # partition the dataset
        sort_dataset = partition_method['sort_dataset']
        if sort_dataset:  # sort by label
            sorting = np.argsort(y_train)
            x_train = x_train[sorting]
            y_train = y_train[sorting]

        x_train_split = np.array_split(x_train, n_nodes)
        y_train_split = np.array_split(y_train, n_nodes)

    elif split_type == 'gaussian_per_node':
        std, train_dataset_size, repetition = partition_method['std'], partition_method['train_dataset_size'], partition_method['repetition']
        distributions = gaussian_per_node(n_nodes, std)
        for distribution in distributions:
            if len(distribution) != len(classes):
                raise KeyError(f'The length of each distribution should be equal to the number of classes')
            pick_index = choose_index(train_dataset_size, class_to_index, distribution, repetition)
            x_train_local, y_train_local = x_train[pick_index], y_train[pick_index]
            x_train_split.append(x_train_local)
            y_train_split.append(y_train_local)

    else:
        raise ValueError('The name of the partition method is unknown')

    # Shuffling and batching
    train_datasets, test_dataset = [], None
    for x, y in zip(x_train_split, y_train_split):
        buff_size = y.shape[0]
        if categorical:  # Transform in one-hot encoding
            y = to_categorical(y, num_classes=10)
        dataset = tf.data.Dataset.from_tensor_slices((x, y))
        dataset = dataset.shuffle(buffer_size=buff_size).batch(batch_size)
        train_datasets.append(dataset)
    test_dataset = tf.data.Dataset.from_tensor_slices((x_test, y_test))
    test_dataset = test_dataset.batch(batch_size)

    return train_datasets, test_dataset


def gaussian_per_node(n_nodes, std, n_classes=10):
    """ Generate a distribution with size points per node, with the distribution in each node 
    being a gaussian of std 'std'.
    Modes:
        'round_robin': the gaussian is centered in index_node % num_classes
            E.g: if 10 classes, then the distribution is centered in 1 in node 1, in 2 in node 2, ..., in 1 for node 11
    """

    distributions = []
    for n in range(n_nodes):
        mean = n % n_classes
        rv = norm(loc=mean, scale=std)

        d = [0] * n_classes
        for i in range(-10 * n_classes, 10 * n_classes):
            d[i % n_classes] += rv.pdf(i)

        d = np.array(d)
        d = d / d.sum()

        distributions.append(tuple(d))
    return distributions
