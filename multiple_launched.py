import multiprocessing as mp
import copy

from utils import check_correctness
from main import launch

import json
from os import listdir
from os.path import isfile, join

args = ['exp_name',
        'n_nodes', 'topology_name', 'model_name', 'lr',
        'n_epochs', 'repetitions', 'proj', 'radius',
        'dataset_name', 'scale', 'std', 'train_dataset_size']

run = {
    'title': 'MWU.',
    'csv_logger': False,
    'n_nodes': 4,
    'topology': {
        'name': 'grid',
      #  'degree': 4
    },
    'model': {
        'name': 'simple_MNIST_convnet',
        'lr': 1e-3,
        # 'momentum': 1
       # 'r' : 0
    },
    'dataset': {
        'name': 'mnist',
        'scale': False,
        'categorical': True,
        'test_dataset_size': 1000,
        'batch_size': 64,
        'partition_method': {
             'name': 'partition',
             'sort_dataset': True
         }
        #'partition_method': {
        #    'name': 'different_uniform',  # or 'same_objective'
        #    'train_dataset_size': 10000,
        #    'n_dimensions': 100,
        #    'max_norm': 10
        #}
        #'partition_method': {
        #    'name': 'gaussian_per_node',
        #    'train_dataset_size': 2000,
        #    'std': 0.1,
        #    'repetition': False,
        #    'n_classes': 10
        #}
        # 'partition_method': {
        #     'name': 'partition',
        #     'sort_dataset': True
        # }
    },
    'general': {
        'n_epochs': 30,
        'MWU': 'epoch', #'average', 'batch', 'epoch',
        'repetitions': 1,
        'projection': False,
        'radius': 0,
        'proj_to_epoch': -1,
        'steps_before_gossip': -1,
        'initialize_after_gossip': False
    }
}

# Create experiment settings
exp = []

for n_nodes in [4]:
    new_run = copy.deepcopy(run)
    new_run['n_nodes'] = n_nodes
    exp .append(new_run)

# Check that there is no typo in names of topology, model, dataset
for run in exp:
    topo_name = run['topology']['name']
    model_name = run['model']['name']
    dataset_name = run['dataset']['name']
    if not check_correctness(topo_name, model_name, dataset_name):
        raise ValueError(f'{topo_name}, {model_name} or {dataset_name} is not a valid name')
#    with mp.Pool(1) as processes:
 #       processes.map(launch, exp)
launch(exp[0])

print(f'DONE')
