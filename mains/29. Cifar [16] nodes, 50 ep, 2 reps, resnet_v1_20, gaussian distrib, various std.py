from Logger import Logger
from tensorflow import keras
import tensorflow as tf

import constants
import dataset_formatter
import models
import topologies as tp
import utils
from DSGD import DSGD


n_nodes = 16
dataset_name = 'cifar10'
topology_name = 'ring'

n_epochs = 50
repetitions = 2

projections = [False]
dividers = [1]



# launched in parallel with [0.1, 0.5], [1, 2] and [10]
std_list = [0.1, 0.5, 1, 2, 10]


train_dataset_size = 100
lr = 10**-4

for std in std_list:
    distrib = dataset_formatter.gaussian_per_node(n_nodes, size=train_dataset_size, n_classes=10, std=std)
    scale = True

    graph = tp.get_graph(topology_name, n=n_nodes)
    train_datasets, test_dataset = dataset_formatter.get_dataset_for_nodes(dataset_name, n_nodes, categorical=True, scale=scale, distributions=distrib)

    model_getter = models.get_resnet_v1(input_shape=(32,32,3), depth=20, lr=lr)
    metric_getter = keras.metrics.CategoricalAccuracy  

    base_norm = 0
    for proj in projections:
        for div in dividers:
            rad = base_norm / div
            logger = Logger()
            logger.log_metadata(
                exp_name='29. Cifar [16] nodes, 50 ep, 2 reps, resnet_v1_20, gaussian distrib, various std',
                num_nodes=n_nodes,
                num_epochs=n_epochs,
                dataset_name=dataset_name,
                topology=topology_name,
                model_name='resnet_v1_20',
                data_iid=None,
                projection=proj,
                radius=rad,
                description=f'base_norm: {base_norm:.3f}, scale: {scale}, std: {std}',
                distribution=f"gaussian_per_node({n_nodes}, size={train_dataset_size}, n_classes=10, std={std}, lr={lr})"
            )

            tf.random.set_seed(constants.SEED)  # set seed before each batch of repetition
            for rep in range(repetitions):
                print(f'Projection {proj}, repetition {rep}, {n_nodes} nodes')
                logger.currentRepetition = rep
                nodes, average_node = utils.fill_nodes(n_nodes, model_getter, train_datasets, test_dataset, metric_getter, logger)
                dsgd = DSGD(n_nodes, ball_projection=proj, ball_radius=rad, logger=logger)
                dsgd.set_graph(graph)
                dsgd.set_nodes(nodes)
                dsgd.set_average_node(average_node)
                dsgd.train(n_epochs)

                logger.save()

            # Do one radius if no projection 
            if proj is False:
                base_norm = dsgd.get_average_node_norm()
                logger.set_basenorm(base_norm)
                logger.save()
                break
