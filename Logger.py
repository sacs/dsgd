import json
import os
from datetime import datetime
import random

import telegram
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

import constants


class Logger:
    def __init__(self):
        self.data = {
            'metadata': dict(),
            'data': {
                'node_name': [],
                'epoch': [],
                'metric_name': [],
                'value': [],
                'repetition': []
            },
        }
        self.currentRepetition = 0
        self.has_metadata = False
        self.dir_path = constants.SAVE_FOLDER
        self.filename = None

    @staticmethod
    def get_date():
        return datetime.now().strftime("%d_%m %H_%M_%S")

    def set_basenorm(self, value):
        self.data['metadata']['base_norm'] = value

    def log_metadata(self, run, base_norm=0):
        self.has_metadata = True
        self.data['metadata'] = {
            'date': Logger.get_date(),
            'base_norm': base_norm,
            'run': run
        }
        self.dir_path = os.path.join(constants.SAVE_FOLDER, run['title'])

    def log(self, node, metric_name, value):
        self.log_basic(node.node_name, node.num_epoch, metric_name, value)

    def log_basic(self, node_name, num_epoch, metric_name, value):
        # Every list of 'logs' should be updated!
        logs = self.data['data']
        logs['node_name'].append(node_name)
        logs['epoch'].append(num_epoch)
        logs['metric_name'].append(metric_name)
        logs['value'].append(value)
        logs['repetition'].append(self.currentRepetition)

    def generate_filename(self):
        pid = str(os.getpid())
        if self.has_metadata:
            meta = self.data['metadata']
            date = meta['date']
            dataset_name = meta['run']['dataset']['name']
            num_nodes = meta['run']['n_nodes']
            model_name = meta['run']['model']['name']
            topology = meta['run']['topology']['name']
            projection = meta['run']['general']['projection']
            radius = meta['run']['general']['radius']
            lr = meta['run']['model']['lr']
            partition = meta['run']['dataset']['partition_method']
            scale = meta['run']['dataset']['scale']
            random_number = str(random.randint(0, 1e6))

            name = f"{date}, {dataset_name}, {num_nodes} nodes, model {model_name}, topo {topology}, scale {scale}, proj {projection}, radius {radius:.2f}, lr {lr:.2f}, pid {pid}, random {random_number}"
            return name + '.json'
        else:
            return f'logger_no_name_{pid}.json'

    def save(self):
        if not self.filename:
            self.filename = self.generate_filename()

        os.makedirs(self.dir_path, exist_ok=True)
        pathname = os.path.join(self.dir_path, self.filename)

        with open(pathname, 'w') as outfile:
            json.dump(self.data, outfile)

    def send_graph(self):
        bot_token = '2106331034:AAHDRXXGtUs6-BaWSwStDQ51rVuOdZqDTBg'
        bot_chatID = '1349889598'
        path = f'{os.getpid()}.jpg'
        
        try:
            fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(10, 5))
            df = pd.DataFrame(self.data['data'])
            tmp = df[df['metric_name'] == 'test_accuracy']

            # Filter first node and average node
            tmp = tmp[tmp['node_name'].isin(['node_0', 'average_node'])]
            # Use this column as hue if you want to show distinct repetitions

            sns.lineplot(x='epoch', y='value', data=tmp, hue='node_name', ax=ax)
            ax.set_ylim(0, 1)
            ax.set_title(self.data['metadata']['date'])
            ax.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)

            plt.savefig(path, dpi=300)

            bot = telegram.Bot(token=bot_token)

            txt = '=================\n=================\n'
            for k, v in self.data['metadata'].items():
                txt += f'{k}: {v} \n'

            bot.send_message(chat_id=bot_chatID, text=txt)
            bot.send_photo(chat_id=bot_chatID, photo=open(path, 'rb'))
            os.remove(path)

        except:
            bot = telegram.Bot(token=bot_token)
            bot.send_message(chat_id=bot_chatID, text="Error sending data")
            
