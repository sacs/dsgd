import time
import numpy as np
import sys
from tqdm import tqdm
import tensorflow as tf
from matplotlib import pyplot as plt
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '1'
import topologies as tp
import dataset_formatter
class DSGD:
    def __init__(self, number_nodes=1, ball_projection=False, ball_radius=1, proj_to_epoch=-1, logger=None):
        self.n_nodes = number_nodes
        self.graph = None
        self.nodes = None
        self.n_epochs = None
        self.current_epoch = None

     #   self.acc=[]
     #   self.fnode=[]


        self.averaging_weights = None
        self.n_batches = None
        self.average_node = None

        self.ball_projection = ball_projection
        self.ball_radius = ball_radius
        self.ball_projection_to_epoch = proj_to_epoch  # do the projection while epoch < self.ball_projection_to_epoch or if self.ball_projection_to_epoch < 0
        self.logger = logger

        self.consensus_distance_paper = False  # PARAMETER HERE
        if self.consensus_distance_paper:
            (x_train, y_train), _ = dataset_formatter.get_numpy_from_tfds('cifar10', scale=True, categorical=True)
            self.x_train_total = x_train
            self.y_train_total = y_train

    def set_nodes(self, nodes):
        if len(nodes) != self.n_nodes:
            raise ValueError('The number of nodes provided is not the same as declared')
        self.nodes = nodes
        self.compute_number_batches()

    def set_average_node(self, node):
        self.average_node = node

    def centr(self,graph):
        a=nx.to_numpy_matrix(self.graph)
        #nx.draw(self.graph,with_labels=True)
        #plt.show()
        adj=self.averaging_weights
        for i in range(len(a)):
            adj[i]=np.ravel(a[i].T)
        centrality= nx.betweenness_centrality(self.graph)
        for i in range(len(centrality)):
            centrality[i]+=0.03
        for i in range(len(self.averaging_weights)):
            s=0
            for j in range(len(self.averaging_weights)):
                if (i==j or adj[i][j]==1):
                    self.averaging_weights[i][j]=1/centrality[j]
                s+=self.averaging_weights[i][j]
            for j in range(len(self.averaging_weights)):
                self.averaging_weights[i][j]=self.averaging_weights[i][j]/s
        return self.averaging_weights

    def computeNewMatrix(self,listOfAcc):
        exp=2.71
        for i in range(len(self.averaging_weights)):
            s=0
            for j in range(len(self.averaging_weights)):
                self.averaging_weights[i][j]= self.averaging_weights[i][j]*(exp**(-1+listOfAcc[j]))
                s+=self.averaging_weights[i][j]
            for j in range(len(self.averaging_weights)):
                self.averaging_weights[i][j]=self.averaging_weights[i][j]/s

        return self.averaging_weights

    def set_graph(self, graph):
        self.graph = graph
        self.averaging_weights = tp.adj_norm(self.graph).astype(np.float64)

    def save_logger(self):
        if self.logger is not None:
            self.logger.save()

    def compute_number_batches(self):
        """
        Compute the minimum number of batches of batches on the various nodes.
        As the parameters are exchanged after each batch training, it is important to train with the same
        number of batches.
        """
        mini = self.nodes[0].get_number_batches()
        for node in self.nodes:
            local_num = node.get_number_batches()
            mini = min(mini, local_num)
        self.n_batches = mini

    def train(self, n_epochs,MWU):
        self.n_epochs = n_epochs
        # Evaluate the model before training
        self.validation_step(-1)

        for epoch in range(n_epochs):
            self.current_epoch = epoch
            start_time = time.time()
            print("\nStart of epoch {}".format(epoch + 1))

            # Set new dataset to each node
            if self.consensus_distance_paper:
                BATCH_SIZE = 64
                train_dataset_size = len(self.x_train_total)
                shuffled_index = np.arange(train_dataset_size)
                np.random.shuffle(shuffled_index)  # Shuffling happens here
                x_train_shuffled = self.x_train_total[shuffled_index]
                y_train_shuffled = self.y_train_total[shuffled_index]
                train_dataset_x = np.array_split(x_train_shuffled, self.n_nodes)
                train_dataset_y = np.array_split(y_train_shuffled, self.n_nodes)

                for i in range(self.n_nodes):
                    dataset = tf.data.Dataset.from_tensor_slices((train_dataset_x[i], train_dataset_y[i]))
                    dataset = dataset.batch(BATCH_SIZE)
                    self.nodes[i].train_dataset = dataset

                self.compute_number_batches()
                print('Train datasets updated')

            # Initialize metrics and iterators
            for node in self.nodes:
                node.initialize_epoch(num_epoch=epoch)

            # Is there still a projection ?
            if 0 <= self.ball_projection_to_epoch <= epoch:
                self.ball_projection = False

            # Perform training and exchange of parameters
            for _ in tqdm(range(self.n_batches), desc='Training (# batches processed)', file=sys.stdout):
                for node in self.nodes:
                    node.train_by_batch()
                    if (MWU == 'batch'):
                        listOfAcc = self.validation_step(epoch)
                        new_adj_matrix = self.computeNewMatrix(listOfAcc)
                        self.averaging_weights = new_adj_matrix
                    #    print(listOfAcc, self.averaging_weights[1])
                self.average_weights()

            # log consensus distance
            consensus_distance = compute_consensus_distance(self.average_node.model, [n.model for n in self.nodes])
            self.logger.log_basic('-1', epoch, 'consensus_distance', consensus_distance)

            # Save metrics in logger
            for node in self.nodes:
                node.end_epoch()


            listOfAcc = self.validation_step(epoch)
            if (MWU=='epoch'):
                new_adj_matrix = self.computeNewMatrix(listOfAcc)
                self.averaging_weights = new_adj_matrix
                #print(listOfAcc,self.averaging_weights[1])
            self.log_model_norms()

            # Save metrics in file
            self.save_logger()

            # Printing time
            duration = time.time() - start_time
            print('Time taken: {:.2f} seconds'.format(duration))
            accuracy = self.nodes[0].val_accuracy_list[-1]
            print('Accuracy on first node: {:3f}'.format(accuracy))
            accuracy_average_node = self.average_node.val_accuracy_list[-1]
            print('Accuracy on average node: {:3f}'.format(accuracy_average_node))
            # self.fnode.append(float(format(accuracy)))
            # self.acc.append(float(format(accuracy_average_node)))
           # print(self.fnode,self.acc,'accuracy of first node then accuracy of average node')
            if (epoch==n_epochs-1):
                plt.plot(self.acc)
                plt.show()




    def validation_step(self, epoch):
        """
        Evaluate the model of each node with their test dataset
        """
        # Validation on first node only
        result=[]
        for node in tqdm(self.nodes, desc='Validation (# nodes processed)', file=sys.stdout):
            result.append(node.do_validation(epoch))
        self.average_node.do_validation(epoch)
        return result
    def get_average_node_norm(self):
        """
        Return the norm of the average node
        """
        return get_model_norm(self.average_node.model.get_weights())

    def log_model_norms(self):
        """
        Log the norm of the models at the end of each epochs
        """
        norm = get_model_norm(self.average_node.model.get_weights())
        self.logger.log_basic(self.average_node.node_name, self.current_epoch, 'model_norm', norm)

    @staticmethod
    def project_ball(w, r):
        """
        Projects the weights w in a ball of radius r
        """
        w_norm = get_model_norm(w)
        if w_norm > r:  # Renormalize if necessary
            for x in range(np.shape(w)[0]):
                w[x] = w[x] * r / w_norm
        return w

    def average_weights(self):
        """
        Perform weight averaging, taking the topology into account
        """
        # temp = np.array([node.model.get_weights() for node in self.nodes], dtype=object)
        # res = np.transpose(np.matmul(np.transpose(temp), self.averaging_weights))

        temp = np.array([node.model.get_weights() for node in self.nodes], dtype=object)
        res = (temp.T @ (self.averaging_weights * self.n_nodes)).T / self.n_nodes
        for j in range(self.n_nodes):
            t = res[j]
            if self.ball_projection:
                t = DSGD.project_ball(t, self.ball_radius)
                res[j] = t
            self.nodes[j].model.set_weights(t)

        self.average_node.model.set_weights((np.ones(self.n_nodes).T @ res) / float(self.n_nodes))

    def average_weights_new_version(self):
        """ Written to check the correctness of the average_weights function. ok but slower."""
        models_parameters = [node.model.get_weights() for node in self.nodes]
        models_parameters_updated = compute_gossip(self.n_nodes, models_parameters, self.averaging_weights)
        if self.ball_projection:
            models_parameters_updated = project_models(models_parameters_updated, self.ball_radius)
        average_model_parameters_updated = compute_average_model(self.n_nodes, models_parameters_updated)

        for node, parameters in zip(self.nodes, models_parameters_updated):
            node.model.set_weights(parameters)
        self.average_node.model.set_weights(average_model_parameters_updated)


def compute_gossip(n_nodes, models_parameters, averaging_weights):
    averaging_weights *= n_nodes
    new_models_parameters = []
    for neighbors in averaging_weights:
        new_params = sum_models(models_parameters, weights=neighbors)
        new_params = [w / n_nodes for w in new_params]
        new_models_parameters.append(new_params)
    return new_models_parameters


def project_models(models_parameters, radius):
    new_parameters = [DSGD.project_ball(parameters, radius) for parameters in models_parameters]
    return new_parameters


def compute_average_model(n_nodes, models_parameters):
    sum_model_parameters = sum_models(models_parameters)
    average_model_parameters = [layer / n_nodes for layer in sum_model_parameters]
    return average_model_parameters


def sum_models(models_parameters, weights=None):
    if weights is not None:
        assert len(models_parameters) == len(weights)
    else:
        weights = np.ones(shape=len(models_parameters))

    sum_parameters = [np.zeros(shape=layer.shape) for layer in models_parameters[0]]
    for param, ponderation in zip(models_parameters, weights):
        for i, layer in enumerate(param):
            sum_parameters[i] += ponderation * layer
    return sum_parameters


def compute_consensus_distance(average_model, models):
    """
        Consensus distance : d = \sqrt {1/n \sum (x_i - x_average)^2}
        """
    distance = 0
    average = np.array(average_model.get_weights(), dtype=object)
    for m in models:
        model = np.array(m.get_weights(), dtype=object)
        w = model - average
        distance += sum([np.linalg.norm(layer) ** 2 for layer in w])
    distance /= len(models)
    return np.sqrt(distance)


def get_model_norm(weights):
    w = weights
    w_norm = 0
    for i in range(len(w)):  # For every layer
        w_norm += np.linalg.norm(w[i]) ** 2  # Take the norm of its weights
    w_norm = np.sqrt(w_norm)
    return w_norm
