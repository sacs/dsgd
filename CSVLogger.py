import json
import os
from datetime import datetime
import random
import pandas as pd
import constants

# Offers exactly same API as Logger class
# except set_basenorm and send_graph methods.
# Saves one csv per repetition containing 
# data of all nodes
class CSVLogger:
    def __init__(self):
        '''
            JSON Format
            {
                1:     # Repetition 1
                {
                    node_1:     # Node 1
                    {
                        val_accuracy: []
                        train_accuracy: []
                        val_loss: []
                        train_loss: []
                    }
                    node_2:     # Node 2
                    {
                        ...
                    }
                }
                2:      # Repetition 2
                {

                }
            }
        '''
        self.data = {}
        self.currentRepetition = 0
        self.dir_path = constants.SAVE_FOLDER
        self.saved_repetition = -1

    @staticmethod
    def get_date():
        return datetime.now().strftime("%d_%m %H_%M_%S")

    # Method for compatiblity with Logger
    # Doesn't actually save any metadata
    # Metadata is instead saved in params.csv
    # based on command line arguments
    def log_metadata(self, run, base_norm=0):
        self.dir_path = os.path.join(constants.SAVE_FOLDER, run['title'])

    def log(self, node, metric_name, value):
        self.log_basic(node.node_name, node.num_epoch, metric_name, value)

    # Make sure all attributes are logged on every epoch
    # Logger does not check for it
    def log_basic(self, node_name, num_epoch, metric_name, value):
        # Check if current repetition has been created
        if self.currentRepetition not in self.data:
            self.data[self.currentRepetition] = {}
        rep_dict = self.data[self.currentRepetition]

        # Check if node exists
        if node_name not in rep_dict:
            rep_dict[node_name] = {}
        node_dict = rep_dict[node_name]

        # Check if property exists
        if metric_name not in node_dict:
            # insert extra zeros for epoch = -1
            # only for metrics other than test_loss and test_accuracy
            if 'test' not in metric_name:
                node_dict[metric_name] = [0]
            else:
                node_dict[metric_name] = []
        node_dict[metric_name].append(value)

    # This function should be called after each repetition
    def save(self):
        # Delete data corresponding to last saved repetition
        if self.saved_repetition in self.data:
            del self.data[self.saved_repetition]
        
        self.saved_repetition += 1
        rep_index = self.saved_repetition
        log_filename = f'logs_rep_{rep_index}.csv'
        log_file = os.path.join(self.dir_path, log_filename)
        rep_data = self.data[rep_index]

        pd_dict = {}

        for node in rep_data:
            node_data = rep_data[node]
            for metric in node_data:
                node_metric_str = node + '_' + metric
                pd_dict[node_metric_str] = node_data[metric]
        
        print(pd_dict)
        df = pd.DataFrame(pd_dict)
        df.to_csv(log_file, index=None)