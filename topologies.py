from math import sqrt
import random
from bisect import bisect_left
from itertools import accumulate, product

import networkx as nx
import numpy as np

topologies_list = dict()


def register_topology(name):
    """ 
    Decorator that add all the topologies in the global dictionnary "topologies_list"
    """
    global topologies_list

    def f(func):
        topologies_list[name] = func
        return func

    return f


def is_topology(name):
    global topologies_list
    return name in topologies_list


def get_graph(name, *args, **kwargs):
    """
    Get a graph based on the name of the topology
    """
    if name not in topologies_list:
        raise ValueError(f'Topology not implemented: {name}')
    return topologies_list[name](*args, **kwargs)


@register_topology('ring')
def ring(n_nodes):
    return nx.cycle_graph(n_nodes)


# Create a grid graph of size n_nodes
@register_topology('grid')
def grid(n_nodes, torus=False):
    if not sqrt(n_nodes).is_integer():
        raise ValueError('Number of nodes should be perfect square')
    n_nodes = int(sqrt(n_nodes))
    return nx.grid_graph(dim=[n_nodes, n_nodes], periodic=torus)


# Create a Kleinberg type graph of size n_nodes*n_nodes, with a long links and parameter distance r
@register_topology('kleinberg')
def kleinberg(n_nodes, p=1, q=1, r=2, gr=42):
    return nx.navigable_small_world_graph(n_nodes, p, q, r, dim=2, seed=gr)


# Create a complete graph of size n_nodes
@register_topology('complete')
def complete(n_nodes):
    return nx.complete_graph(n_nodes)


# Add a configuration model graph
@register_topology('d_regular')
def random_regular(n_nodes, degree, seed=None):
    """
    Return a connected d-regular graph (each node has d neighbors). No self-loops, no parallel edges
    """
    if n_nodes > 2 and degree == 1:
        raise ValueError('Not possible to get a connected graph with degree = 1 and n_nodes > 2')
    if n_nodes == 1 and degree != 0:
        raise ValueError('Not possible to get a graph with self loops')

    if n_nodes == 1:
        g = nx.Graph()
        g.add_node(1)
        return g

    for _ in range(500000):
        g = nx.random_regular_graph(degree, n_nodes, seed)
        if nx.is_connected(g):
            return g
    raise ValueError('Connected graph not found with the given parameters')


# Create the normalized adjacency matrix of a graph where all neighbours have the same weight
def adj_norm(G):
    n = G.number_of_nodes()
    A = nx.to_numpy_array(G)
    A = A + np.identity(n)
    row_sums = A.sum(axis=1)
    return A / row_sums[:, np.newaxis]


# Kleinberg graphs with possible negative r
@register_topology('kleimberg_2')
def kleinberg_2(n_nodes, p=1, q=1, r=2, dim=2, seed=42):
    random.seed(seed)
    if p < 1:
        raise nx.NetworkXException("p must be >= 1")
    if q < 0:
        raise nx.NetworkXException("q must be >= 0")

    G = nx.DiGraph()
    nodes = list(product(range(n_nodes), repeat=dim))
    for p1 in nodes:
        probs = [0]
        for p2 in nodes:
            if p1 == p2:
                continue
            d = sum((abs(b - a) for a, b in zip(p1, p2)))
            if d <= p:
                G.add_edge(p1, p2)
            probs.append(d ** -r)
        cdf = list(accumulate(probs))
        for _ in range(q):
            target = nodes[bisect_left(cdf, random.uniform(0, cdf[-1]))]
            G.add_edge(p1, target)
    return G
