import tensorflow as tf
import collections
import numpy as np

def get_stacked_rnn():
    vocab_len = 400000
    seq_len = 25
    embedding_dim = 100
    model = tf.keras.models.Sequential()
    model.add(tf.keras.layers.Embedding(vocab_len + 1, embedding_dim, 
        input_length=seq_len))
    rnn_cells = [tf.keras.layers.LSTMCell(100) for _ in range(2)]
    stacked_rnn = tf.keras.layers.StackedRNNCells(rnn_cells)
    model.add(tf.keras.layers.RNN(stacked_rnn))
    model.add(tf.keras.layers.Dense(30, activation='relu'))
    model.add(tf.keras.layers.Dense(1, activation='sigmoid'))
    
    return model

def get_synthetic_perceptron():
    return tf.keras.Sequential([
        tf.keras.layers.InputLayer(input_shape=(60,)),
        tf.keras.layers.Dense(5, activation='softmax')
    ])

# Batch Normalisation is removed since its not clear how to port moving_mean
# and moving_variance from clients to server
# Also facing other technical issues with evaluating keras model with BatchNorm layers
def get_celeba_cnn():
    return tf.keras.Sequential([
        tf.keras.layers.Conv2D(32, (3, 3), padding='same', input_shape=(84, 84, 3)),
        # tf.keras.layers.BatchNormalization(),
        tf.keras.layers.MaxPool2D(2, 2, padding='same'),
        tf.keras.layers.ReLU(),
        tf.keras.layers.Conv2D(32, (3, 3), padding='same'),
        # tf.keras.layers.BatchNormalization(),
        tf.keras.layers.MaxPool2D(2, 2, padding='same'),
        tf.keras.layers.ReLU(),
        tf.keras.layers.Conv2D(32, (3, 3), padding='same'),
        # tf.keras.layers.BatchNormalization(),
        tf.keras.layers.MaxPool2D(2, 2, padding='same'),
        tf.keras.layers.ReLU(),
        tf.keras.layers.Conv2D(32, (3, 3), padding='same'),
        # tf.keras.layers.BatchNormalization(),
        tf.keras.layers.MaxPool2D(2, 2, padding='same'),
        tf.keras.layers.ReLU(),
        tf.keras.layers.Flatten(),
        tf.keras.layers.Dense(2, activation='softmax'),
    ])

def get_femnist_cnn():
    return tf.keras.Sequential([
        tf.keras.layers.Conv2D(32, (5, 5), input_shape=(28, 28, 1)),
        tf.keras.layers.MaxPool2D(2, 2),
        tf.keras.layers.Conv2D(64, (5, 5)),
        tf.keras.layers.MaxPool2D(2, 2),
        tf.keras.layers.Flatten(),
        tf.keras.layers.Dense(512, activation='relu'),
        tf.keras.layers.Dense(62, activation='softmax')
    ])

def get_stacked_lstm():
    # 86 is vocab len
    model = tf.keras.models.Sequential()
    model.add(tf.keras.layers.Embedding(86, 8, input_length=80))
    rnn_cells = [tf.keras.layers.LSTMCell(256) for _ in range(2)]
    stacked_lstm = tf.keras.layers.StackedRNNCells(rnn_cells)
    model.add(tf.keras.layers.RNN(stacked_lstm))
    model.add(tf.keras.layers.Dense(86, activation='softmax'))
    return model
