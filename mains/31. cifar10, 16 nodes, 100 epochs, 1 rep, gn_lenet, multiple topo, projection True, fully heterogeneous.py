from tensorflow.python.eager.context import scope_name
from Logger import Logger
from tensorflow import keras
import tensorflow as tf
import numpy as np

import constants
import dataset_formatter
import models
import topologies as tp
import utils
from DSGD import DSGD


n_nodes_list = [16] 
scale_list = [True]
dataset_name = 'cifar10'
topology_name = 'grid' # Launched in parallel with 'ring'

n_epochs = 100
repetitions = 1

proj_list = [False, True]
dividers = [2]

std = 0.001
train_dataset_size = 5000
lr = 3*(10**-3) # launched in parallel with 5* 10**-5

for n in n_nodes_list:
    graph = tp.get_graph(topology_name, n=n)
    n_nodes = graph.number_of_nodes()
    distrib = dataset_formatter.gaussian_per_node(n_nodes, size=train_dataset_size, n_classes=10, std=std)

    for scale in scale_list:

        train_datasets, test_dataset = dataset_formatter.get_dataset_for_nodes(dataset_name, n_nodes, categorical=True, scale=scale, distributions=distrib)

        model_getter = models.get_gn_lenet(lr=lr)
        metric_getter = keras.metrics.CategoricalAccuracy  

        base_norm = 0
        for proj in proj_list:
            for div in dividers:
                rad = base_norm / div
                logger = Logger()
                logger.log_metadata(
                    exp_name='31. cifar10, 16 nodes, 100 epochs, 1 rep, gn_lenet, multiple topo, projection True, fully heterogeneous',
                    num_nodes=n_nodes,
                    num_epochs=n_epochs,
                    dataset_name=dataset_name,
                    topology=topology_name,
                    model_name='get_gn_lenet',
                    data_iid=None,
                    projection=proj,
                    radius=rad,
                    description=f'base_norm: {base_norm:.3f}, scale: {scale}, std: {std}, lr: {lr}',
                    distribution=f"gaussian_per_node({n_nodes}, size={train_dataset_size}, n_classes=10, std={std})"
                )

                tf.random.set_seed(constants.SEED)  # set seed before each batch of repetition
                for rep in range(repetitions):
                    print(f'Projection {proj}, repetition {rep}, {n_nodes} nodes')
                    logger.currentRepetition = rep
                    nodes, average_node = utils.fill_nodes(n_nodes, model_getter, train_datasets, test_dataset, metric_getter, logger)
                    dsgd = DSGD(n_nodes, ball_projection=proj, ball_radius=rad, logger=logger)
                    dsgd.set_graph(graph)
                    dsgd.set_nodes(nodes)
                    dsgd.set_average_node(average_node)
                    dsgd.train(n_epochs)

                    logger.save()
                
                
                logger.send_graph()  # send graph at the end of repetitions

                # Do one radius if no projection 
                if proj is False:
                    base_norm = dsgd.get_average_node_norm()
                    logger.set_basenorm(base_norm)
                    logger.save()
                    break
