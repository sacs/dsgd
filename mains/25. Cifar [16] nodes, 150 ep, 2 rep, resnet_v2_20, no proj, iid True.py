from Logger import Logger
from tensorflow import keras
import tensorflow as tf

import constants
import dataset_formatter
import models
import topologies as tp
import utils
from DSGD import DSGD

def lr_decay(epoch):
    lr = 5*(10**-4)
    if epoch < 30:
        lr /= 5
    elif epoch < 40:
        lr /= 10
    elif epoch < 70:
        lr /= 50
    elif epoch < 100:
        lr /= 100
    else:
        lr /= 500
    return lr
 
list_n = [16]

for n in list_n:
    n_epochs = 150
    graph = tp.ring(n)
    n_nodes = graph.number_of_nodes()

    scale = True
    data_iid = True
    repetitions = 2
    lr = 10**-4

    projections = [False]
    dividers = [1]

    train_datasets, test_dataset = dataset_formatter.get_cifar10_for_nodes(n_nodes, iid=data_iid, scale=scale)

    model_getter = models.get_resnet_v2(input_shape=(32,32,3), depth=20, lr=lr)
    metric_getter = keras.metrics.CategoricalAccuracy  

    base_norm = 0
    for proj in projections:
        for div in dividers:
            rad = base_norm / div
            logger = Logger()
            logger.log_metadata(
                exp_name='25. Cifar [16] nodes, 150 ep, 2 rep, resnet_v2_20, no proj, iid True',
                num_nodes=n_nodes,
                num_epochs=n_epochs,
                dataset_name='Cifar',
                topology='ring',
                model_name='resnet_v2_20',
                data_iid=data_iid,
                projection=proj,
                radius=rad,
                description=f'base_norm: {base_norm:.3f}, scale: {scale}, lr: decay'
            )

            tf.random.set_seed(constants.SEED)  # set seed before each batch of repetition
            for rep in range(repetitions):
                print(f'Projection {proj}, repetition {rep}, {n_nodes} nodes')
                logger.currentRepetition = rep
                nodes, average_node = utils.fill_nodes(n_nodes, model_getter, train_datasets, test_dataset, metric_getter, logger, lr_decay)
                dsgd = DSGD(n_nodes, ball_projection=proj, ball_radius=rad, logger=logger)
                dsgd.set_graph(graph)
                dsgd.set_nodes(nodes)
                dsgd.set_average_node(average_node)
                dsgd.train(n_epochs)

                logger.save()

            # Do one radius if no projection 
            if proj is False:
                base_norm = dsgd.get_average_node_norm()
                logger.set_basenorm(base_norm)
                logger.save()
                break
