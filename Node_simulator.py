import tensorflow as tf


class NodeSimulator:
    """
    This class is supposed to represent a node, with its proper model, optimizer, loss function, dataset, ...
    """

    def __init__(self, node_name, lr_decay_func=None):
        # For logging purposes
        self.node_name = node_name
        self.lr_decay_func = lr_decay_func
        self.num_epoch = None
        self.logger = None

        # Validation metrics
        self.test_loss = 0
        self.val_accuracy = None
        self.val_accuracy_list = []

        # Training metrics
        self.train_loss = 0
        self.epoch_train_accuracy = None
        self.epoch_train_accuracy_list = []

        # Training essentials
        self.model = None
        self.optimizer = None
        self.loss_fn = None

        # Dataset essentials
        self.train_dataset = None
        self.test_dataset = None
        self.train_ds_iter = None

    def generate_weights(self):
        """
        The weights of the model are generated upon the first forward pass.
        May be necessary to call this method for the average_node.
        """
        x, y = next(iter(self.test_dataset))
        self.model(x, training=False)

    def get_number_batches(self):
        assert self.train_dataset.cardinality() != tf.data.UNKNOWN_CARDINALITY
        return self.train_dataset.cardinality().numpy().item()

    def initialize_epoch(self, num_epoch=-1):
        # Necessary for logging info
        self.num_epoch = num_epoch
        
        # Reset epoch related metrics
        self.epoch_train_accuracy.reset_states()
        self.train_loss = 0
        
        # Reset batch iterator
        self.train_ds_iter = iter(self.train_dataset)
        
        # Update lr of model
        if self.lr_decay_func is not None:
            lr = self.lr_decay_func(num_epoch)
            self.optimizer.lr.assign(lr)

    def end_epoch(self):
        # Save epoch related metrics

        self.train_loss /= self.get_number_batches()
        self.logger.log(self, 'train_loss', self.train_loss.numpy().item())

        result = self.epoch_train_accuracy.result().numpy().item()
        self.logger.log(self, 'train_accuracy', result)

        self.epoch_train_accuracy_list.append(result)

    @tf.function
    def grad(self, x, y):
        with tf.GradientTape() as tape:
            y_pred = self.model(x, training=True)
            loss_value = self.loss_fn(y, y_pred)
        return loss_value, tape.gradient(loss_value, self.model.trainable_variables)

    def train_by_batch(self):
        """
        Performs the model training with the next batch of the training dataset
        """
        x, y = next(self.train_ds_iter)
        loss_value, grads = self.grad(x, y)
        self.optimizer.apply_gradients(zip(grads, self.model.trainable_weights))
        self.train_loss += loss_value

    def do_validation(self, epoch=None):
        """
        Performs validation step using the node test_dataset
        """

        # Check that num_epoch is well set
        # Not the case for average_node
        if epoch is not None:
            self.num_epoch = epoch

        # Reset for this validation
        self.val_accuracy.reset_states()
        self.test_loss = 0

        for x, y in self.test_dataset:
            y_pred = self.model(x, training=False)
            self.val_accuracy.update_state(y, y_pred)
            self.test_loss += self.loss_fn(y, y_pred)

        self.test_loss /= len(self.test_dataset)
        self.logger.log(self, 'test_loss', self.test_loss.numpy().item())

        result = self.val_accuracy.result().numpy().item()
        self.logger.log(self, 'test_accuracy', result)
        
        self.val_accuracy_list.append(result)
        return result

    def initialize_gossip(self):
        if isinstance(self.optimizer, tf.keras.optimizers.SGD):
            self.optimizer = tf.keras.optimizers.SGD(**self.optimizer.get_config())
        elif isinstance(self.optimizer, tf.keras.optimizers.Adam):
            self.optimizer = tf.keras.optimizers.Adam(**self.optimizer.get_config())
