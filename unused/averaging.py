import matplotlib.pyplot as plt
import numpy as np

import topologies as tp


# Given an modified adjacency matrix G of size nxn and a vector X of size n
# Do one averaging step of the graph nodes with parameters X
def one_stp_avg(adjacency_matrix, x):
    a = np.transpose(adjacency_matrix)
    return np.dot(x, a)


def get_consensus_distance(x, n):
    dist_temp = np.linalg.norm(x - np.dot(x, np.ones((n, n)) / float(n)), 'fro')
    return dist_temp


# Do avg steps until the consensus distance of X is smaller than epsilon
# Returns the distances vector, the last vector and the number of steps. X should be a matrix
def dec_avg(graph, x, epsilon):
    counter = 0
    adjacency_matrix = tp.adj_norm(graph)
    n = graph.number_of_nodes()
    dist_temp = get_consensus_distance(x, n)
    distances = [dist_temp]
    while dist_temp > n * epsilon:
        x = one_stp_avg(adjacency_matrix, x)
        counter += 1
        dist_temp = get_consensus_distance(x, n)
        distances.append(dist_temp)
    return distances, x, counter


# Do avg steps for a fixed number of steps m
# Return the distances vector
def dec_avg_m(graph, x, number_steps):
    adjacency_matrix = tp.adj_norm(graph)
    n = graph.number_of_nodes()
    dist_temp = get_consensus_distance(x, n)
    distances = [dist_temp]
    for _ in range(number_steps):
        x = one_stp_avg(adjacency_matrix, x)
        dist_temp = get_consensus_distance(x, n)
        distances.append(dist_temp)
    return distances


def apply_averaging(graph):
    global number_steps, start_values
    return dec_avg_m(graph, start_values, number_steps)


# A small test where we draw consensus distances for grid and kleinberg distribution
# both with r = 2 and r = 0

n = 10
number_steps = 100
start_values = [np.logspace(0.0, n, num=n * n, base=2.)]

topologies = [
    tp.complete(n * n),
    tp.grid(n),
    tp.kleinberg(n, r=0),
    tp.kleinberg(n, r=2)
]

labels = ['Complete', 'Grid', 'Kleinberg r=0', 'Kleinberg r=2']

results = list(map(apply_averaging, topologies))

steps = np.arange(0, number_steps + 1)

for i, l in zip(results, labels):
    plt.plot(steps, i, label=l)
    plt.legend()
plt.show()

# Kleinberg with parameter 0 seems to be the best
